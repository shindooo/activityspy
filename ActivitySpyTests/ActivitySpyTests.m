//
//  ActivitySpyTests.m
//  ActivitySpyTests
//
//  Created by 進藤 孝司 on 2014/02/22.
//  Copyright (c) 2014年 進藤 孝司. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ProcessResearcher.h"

@interface ActivitySpyTests : XCTestCase

@end

@implementation ActivitySpyTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    // XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

- (void)testGetCurrentProcesses
{
    ProcessResearcher *processResearcher = [[ProcessResearcher alloc] init];
    NSArray *processInfoArray = [processResearcher getCurrentProcesses];
    
    XCTAssertTrue([processInfoArray count] > 0, @"一つ以上のプロセス情報を取得できている");
}


@end
