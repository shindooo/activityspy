//
//  ProcessResearcher.h
//  ActivitySpy
//
//  Created by 進藤 孝司 on 2014/02/22.
//  Copyright (c) 2014年 進藤 孝司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sys/sysctl.h>
#import <dlfcn.h>
#import <pwd.h>

@interface ProcessResearcher : NSObject

// 現在起動しているプロセスの情報を返します
// プロセス情報の取得に失敗した場合はnilを返します
- (NSArray*)getCurrentProcesses;

@end
