//
//  BackGroundURLSession.h
//  ActivitySpy
//
//  Created by 進藤 孝司 on 2014/02/23.
//  Copyright (c) 2014年 進藤 孝司. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UploadTaskDelegate.h"

@interface BackGroundURLSession : NSObject

// 引数で渡されたデータをファイルとしてアップロードします
// URLやパラメータ名などの設定は urlSession.plist で行います
- (BOOL)uploadData:(NSData *)data;

@end
