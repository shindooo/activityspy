//
//  ProcessResearcher.m
//  ActivitySpy
//
//  Created by 進藤 孝司 on 2014/02/22.
//  Copyright (c) 2014年 進藤 孝司. All rights reserved.
//

#import "ProcessResearcher.h"

@implementation ProcessResearcher

const NSString *DICT_KEY_PID = @"pid";
const NSString *DICT_KEY_PPID = @"ppid";
const NSString *DICT_KEY_BUNDLE_ID = @"bundle_id";
const NSString *DICT_KEY_PROCESS_NAME = @"process_name";

// 動的ロードする関数へのポインタ
mach_port_t* (*SBSSpringBoardServerPort)(void);
void* (*SBDisplayIdentifierForPID)(mach_port_t* port, int pid,char * result);


- (id)init
{
    if (self = [super init]) {
        [self loadSpringBoardServices];
    }
    return self;
}

// SpringBoardServices(iOSのプライベートフレームワーク)をロード
// ※SpringBoardServicesはバンドルIDを検索するために使用する
- (void)loadSpringBoardServices
{
    void *lib = dlopen("/System/Library/PrivateFrameworks/SpringBoardServices.framework/SpringBoardServices", RTLD_LAZY);
    SBSSpringBoardServerPort = dlsym(lib, "SBSSpringBoardServerPort");
    SBDisplayIdentifierForPID = dlsym(lib, "SBDisplayIdentifierForPID");
    dlclose(lib);
}


- (NSArray*)getCurrentProcesses
{
    NSMutableArray *processArr = [NSMutableArray array];
    size_t bufferSize;
    
    const int MIB_SIZE = 4;
    int mib[MIB_SIZE] = { CTL_KERN, KERN_PROC, KERN_PROC_ALL, 0 };
    // バッファサイズを取得
    if (sysctl(mib, MIB_SIZE, NULL, &bufferSize, NULL, 0) < 0) {
        return nil;
    }
    
    struct kinfo_proc *procs = malloc(bufferSize);
    // プロセス情報取得
    if (sysctl(mib, MIB_SIZE, procs, &bufferSize, NULL, 0) < 0) {
        free(procs);
        return nil;
    }
    
    mach_port_t *port = (*SBSSpringBoardServerPort)();
    int count = (int)(bufferSize / sizeof(*procs));
    for(int i = 0; i < count; ++i) {
        
        pid_t pid = procs[i].kp_proc.p_pid;
        NSString *pidStr = [NSString stringWithFormat:@"%d", pid];
        NSString *ppidStr = [NSString stringWithFormat:@"%d", procs[i].kp_eproc.e_ppid];
        
        // バンドルID取得
        char bundleId[256];
        (*SBDisplayIdentifierForPID)(port, pid, bundleId);
        NSString *bundleIdStr = @(bundleId);
        
        NSString *processNameStr = @(procs[i].kp_proc.p_comm);
        
        NSDictionary * processInfoDict = @{DICT_KEY_PID: pidStr,
                                           DICT_KEY_PPID: ppidStr,
                                           DICT_KEY_BUNDLE_ID: bundleIdStr,
                                           DICT_KEY_PROCESS_NAME: processNameStr};
        
        [processArr addObject:processInfoDict];
    }
    free(procs);
    
    return processArr;
}

@end
