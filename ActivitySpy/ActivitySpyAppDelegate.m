//
//  ActivitySpyAppDelegate.m
//  ActivitySpy
//
//  Created by 進藤 孝司 on 2014/02/22.
//  Copyright (c) 2014年 進藤 孝司. All rights reserved.
//

#import "ActivitySpyAppDelegate.h"

@implementation ActivitySpyAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // バッジ消去
    // [UIApplication sharedApplication].applicationIconBadgeNumber = -1;
    
    // バックグラウンドフェッチの最小間隔を指定
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


// バックグラウンドフェッチで行う処理
-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    // プロセス情報を取得
    ProcessResearcher *processResearcher = [[ProcessResearcher alloc] init];
    NSArray *processInfoArray = [processResearcher getCurrentProcesses];
    
    if (processInfoArray == nil) {
        NSLog(@"%@", @"プロセス情報取得失敗");
        
        // 結果を報告しなければならない
        completionHandler(UIBackgroundFetchResultFailed);
        
        return;
    }
    
    // JSONデータに変換
    NSError* error;
    NSData *data = [NSJSONSerialization dataWithJSONObject:processInfoArray
                                    options:kNilOptions error:&error];
    
    // アップロード
    BackGroundURLSession *bgSession = [[BackGroundURLSession alloc] init];
    BOOL result = [bgSession uploadData:data];
    
    if (!result) {
        NSLog(@"%@", @"IOエラー");
        
        // 結果を報告しなければならない
        completionHandler(UIBackgroundFetchResultFailed);
        
        return;
    }
    
    // 結果を報告しなければならない
    completionHandler(UIBackgroundFetchResultNewData);
    
}

@end
