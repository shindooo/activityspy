//
//  ActivitySpyAppDelegate.h
//  ActivitySpy
//
//  Created by 進藤 孝司 on 2014/02/22.
//  Copyright (c) 2014年 進藤 孝司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProcessResearcher.h"
#import "BackGroundURLSession.h"

@interface ActivitySpyAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
