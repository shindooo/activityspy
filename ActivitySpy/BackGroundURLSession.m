//
//  BackGroundURLSession.m
//  ActivitySpy
//
//  Created by 進藤 孝司 on 2014/02/23.
//  Copyright (c) 2014年 進藤 孝司. All rights reserved.
//

#import "BackGroundURLSession.h"

@implementation BackGroundURLSession

- (BOOL)uploadData:(NSData *)data
{
    // urlSession.plistから情報を読み込み
    NSString *urlSessionPlist = [[NSBundle mainBundle] pathForResource:@"urlSession" ofType:@"plist"];
    NSDictionary *urlSessionInfoDict = [NSDictionary dictionaryWithContentsOfFile:urlSessionPlist];
    
    NSURL *url = [NSURL URLWithString:urlSessionInfoDict[@"URL"]];
    NSString *paramName = urlSessionInfoDict[@"PARAM_NAME"]; // パラメータ名
    NSString *boundary = urlSessionInfoDict[@"BOUNDARY"]; // バウンダリ
    NSString *uploadFile = urlSessionInfoDict[@"UPLOAD_FILE"]; // アップロードファイル名
    NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:uploadFile];
    
    NSString *identifier = urlSessionInfoDict[@"NSURLSESSION_IDENTIFIER"];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration backgroundSessionConfiguration:identifier];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:[[UploadTaskDelegate alloc]init] delegateQueue:nil];
    
    // リクエストボディ
    NSMutableData *body = [[NSMutableData alloc] init];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"uuid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@", [[UIDevice currentDevice].identifierForVendor UUIDString]]
                      dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", paramName, uploadFile]
                      dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:data];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // リクエスト
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    // [request setHTTPBody:body]; リクエストにセットしたボディは無視される
    [request setValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary] forHTTPHeaderField:@"Content-Type"];
    
    // バックグラウンドでのアップロードタスクは uploadTaskWithRequest:fromFile: しか使用できないのでリクエストボディをファイルに格納
    if (![self makeBodyFile:filePath body:body]) {
        return NO;
    }
    
    NSURL *fileUrl = [NSURL fileURLWithPath:filePath];
    
    // アップロードタスク開始
    [[session uploadTaskWithRequest:request fromFile:fileUrl] resume];

    return YES;
}


- (BOOL)makeBodyFile:(NSString *)filePath body:(NSMutableData *)body
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // 既にファイルが存在すれば削除
    if ([fileManager fileExistsAtPath:filePath]){
        if (![fileManager removeItemAtPath:filePath error:nil]){
            return NO;
        };
    }
    
    // ファイルを作成
    return [fileManager createFileAtPath:filePath contents:body attributes:nil];
}

@end
